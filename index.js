require('dotenv').config()

const Discord = require('discord.js');
const bot = new Discord.Client();
const fs = require('fs');

const guidelines = fs.readFileSync('guidelines.txt', 'utf8');
const resources = fs.readFileSync('resources.txt', 'utf8');
const faq = fs.readFileSync('faq.txt', 'utf8');

// Log successful start-up
bot.on('ready', () => {
  console.log(`Logged in as ${bot.user.tag}.`);
});

// Inform of guidelines on-demand
bot.on('message', msg => {
  if (msg.content === '!guidelines') {
    msg.reply(guidelines);
  }

  if (msg.content === '!resources') {
    msg.reply(resources);
  }

  if (msg.content === '!faq') {
    msg.reply(faq);
  }
});

// Share guidelines with all new joiners
bot.on('guildMemberAdd', member => {
  member.send(guidelines);
  member.send(resources);
  member.send(faq);
});

bot.login(process.env.DISCORD_TOKEN);
